/*
    @testClass Teste_Trigger_TestTrigger

    @description Trigger for object: Account

    @date 28/01/2021
    @author André Souza
*/
trigger TestTrigger on Account (before update){
    if(trigger.isUpdate && trigger.isBefore){
        System.debug(LoggingLevel.ERROR, '___ ['+ 'CLASS_NAME' +' - '+ 'METHOD_NAME' +'()] - do some shit: ');
    }
}