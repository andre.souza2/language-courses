/*
    @testClass TestClassName

    @description Handler class for object: 

    @author André Souza - Banco Bari
    @date 27/01/2021
*/
public class ClassName{

    private static String CLASS_NAME = 'ClassName';

    /*
        @description 
            @param String pTest
        @return static void
    
        @author André Souza - Banco Bari
        @date 27/01/2021
    */
    public static void novoMetodo(String pTest){
        String METHOD_NAME = 'novoMetodo';

        System.debug(LoggingLevel.ERROR, '___ ['+ CLASS_NAME +' - '+ METHOD_NAME +'()] - pTest: '+ pTest);
        System.debug(LoggingLevel.ERROR, '___ ['+ CLASS_NAME +' - '+ METHOD_NAME +'()] - Novo test: '+ Novo test);
        
    }
}